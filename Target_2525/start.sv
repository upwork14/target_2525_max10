
`include "defines.sv"

module start_mod
(
	input  bit                		clk_i,
	input  bit                		ce_i,  
	input  bit [CHANNELS - 1:0] 	signal_i,		//	[10:0]
	input  bit							rst_state_i,	//	A_rdy 1mks
	  	
	output bit                		axis_o,
	output channel_state				state_o
	
);

//	=========== local parameters ===========
// =========== internal registers =========
	bit [5:0]   parallel_signals;	
	bit 			rst_state[4];	
	
	int	 		X_counter_min;
	int	 		X_counter_max;
	int	 		Y_counter_min;
	int	 		Y_counter_max;
//=========================================
//	сбросим state_o по срезу сигнала A_rdy (1 mks)
	assign rst_state[3] = (!rst_state[0]) && rst_state[2];

	always_ff @(posedge clk_i)
	begin
		if(!ce_i)
		begin 
			rst_state[0] <= '0;
			rst_state[1] <= '0;
			rst_state[2] <= '0;
			
		end
		else
		begin 
			rst_state[0] <= rst_state_i;
			rst_state[1] <= rst_state[0];
			rst_state[2] <= rst_state[1];			
			
		end
	end
	
//	подсчёт кол-ва одновременных сигналов	
	always_comb
	begin
		parallel_signals = '0;
    
		for (int i = 0; i < CHANNELS; ++i)
			begin
				
				parallel_signals = parallel_signals + signal_i[i];
			end
    
	end

//	 длительность суммарного (одновременно > 4) 
//  сигнала 90..100 mks 105..115 mks
  
  always_ff @(posedge clk_i or negedge ce_i or posedge rst_state[3])
  begin
		if ((!ce_i) || rst_state[3])
		begin    
			X_counter_min	<= X_time_min;
			X_counter_max	<= X_time_max;
			Y_counter_min	<= Y_time_min;
			Y_counter_max	<= Y_time_max;
			state_o 			<= IDLE;
		end 
		else
		begin      			
			if (parallel_signals >= MIN_DIODES)
			begin					
				state_o  <= FIND_START;
				
				if (X_counter_min > 1'b0)
					X_counter_min <= X_counter_min - 1'b1;
				if (X_counter_max > 1'b0)
					X_counter_max <= X_counter_max - 1'b1;
				if (Y_counter_min > 1'b0)
					Y_counter_min <= Y_counter_min - 1'b1;
				if (Y_counter_max > 1'b0)
					Y_counter_max <= Y_counter_max - 1'b1;
			end
			else
			begin					
				if ((X_counter_max > 0) && (X_counter_min == 0))
				begin
					state_o <= COUNT;						
					axis_o  <= 1'b0;	
				end
				else
				if ((Y_counter_max > 0) && (Y_counter_min == 0))
				begin
					state_o <= COUNT;
					axis_o  <= 1'b1;
				end
				else
				begin
					state_o <= IDLE;
					X_counter_min	<= X_time_min;
					X_counter_max	<= X_time_max;
					Y_counter_min	<= Y_time_min;
					Y_counter_max	<= Y_time_max;	
				end
			end
			 		
		end
  end

endmodule
