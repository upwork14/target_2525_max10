
`include "defines.sv"

module spi_module
(	
	input 	logic        clk_48m_in,		//	48MHz
	input 	logic        spi_cs_in,
	input 	logic        spi_clk_in,  
	input 	logic        spi_mosi_in,
	input    logic [2:0]	 mcod_in,		   //	код канала маяка
	input    logic        mget_in,		   //	строб для чтения 
	
	input 	logic [15:0] times0_in [(INPUTS-1):0],		//	маяк_0
	input 	logic [15:0] times1_in [(INPUTS-1):0],		// маяк_1	
	output 	logic			 spi_miso_out
);

//-------------------	local variables --------------------
	bit [2:0]	  spi_cs_sync;
	bit			  spi_clk_sync;	
	bit			  cs_srez, cs_front;
	bit			  busy;
	bit [2:0]	  channel;
		
	bit [7:0]	  cnt_words;	//	считаем words SPI	
	bit [7:0]	  cnt_bits;		//	считаем bits SPI	
	logic [15:0]  word_out;
	
//----------------------------------------------------------
//	синхронизируем сигналы SPI с тактом 50 МГц  
	always_ff @(posedge clk_48m_in)
	begin		
		spi_cs_sync[2] <= spi_cs_sync[1];		
		spi_cs_sync[1] <= spi_cs_sync[0];	
		spi_cs_sync[0] <= spi_cs_in;
		spi_clk_sync   <= spi_clk_in;
	end
	
//	одиночные импульсы синхронизации front & srez	
	assign cs_srez   = (~spi_cs_sync[0])  & spi_cs_sync[2];		// ---|_____----
	assign cs_front  =   spi_cs_sync[0]   & (~spi_cs_sync[2]);	// ----_____|----
	
//----------------------------------------------------------
/* устанaвливаем канал для SPI, организуем счетчик 16-ти битных 
   слов, обнуляем по любому биту mget_in */
	always_ff@( posedge mget_in, posedge cs_front )	
	if ( mget_in )
	begin
		channel   <= mcod_in; // номер маяка для опроса
		cnt_words <= 0;			//	счетчик words в исходное
	end
	else  // увеличиваем по окончанию каждого CS
		if ( cnt_words < INPUTS ) 
			cnt_words = cnt_words + 1; 		

//----------------------------------------------------------
/* формируем выходной сигнал SPI_MISO */	
	always_ff @(posedge cs_srez or posedge spi_clk_sync)
	if ( cs_srez )
	begin
		cnt_bits = 16;	//	бит в слове
		if ( channel == 0 )
	     word_out = times0_in[cnt_words];
		else  
		  word_out = times1_in[cnt_words];//cnt_words+16'hAA00;
	end
	else
	begin		
		spi_miso_out <= word_out[(cnt_bits & 8'hFF)];
		cnt_bits	    <= cnt_bits - 1;
	end	

//----------------------------------------------------------

//	организуем запись данных для SPI
	//always_ff @( posedge spi_clk_sync)
		
	//if ( channel == 0 )
		//spi_miso_out <= word_out[cnt_bit];			
	//else if ( channel == 1 )
	//	spi_miso_out <= times1_in[cnt_byte][cnt_bit];			
	
//----------------------------------------------------------
/*
//	подсчитаем, сколько сигналов измерили и сформируем FPGA_DRDY
	always_comb
	begin
		parallel_signals = 0;//'0;
    
		for (int i=0; i<INPUTS; ++i)	//	24
			begin
				parallel_signals = parallel_signals + data_rdy_in[i];
			end
		if (parallel_signals < N_CHANS)	//	1(test) or 4
			data_rdy_out = 0;
		else
		begin
			data_rdy_out 	= 1;
			
		end
	end
*/
//----------------------------------------------------------
endmodule

