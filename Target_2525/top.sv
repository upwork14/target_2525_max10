`include "defines.sv"

// ================= PIN DESCRIPTION =====================
module top 
(
	input		logic						clk_48m_i, 
	input		logic 					nRF_IRQ_i,		//	--|______|-- from nRF
	input		logic						hall_mayak_i,
	
//	SPI2_interface
	input		logic						spi2_CS_i,
	input		logic						spi2_SCK_i,
	input		logic						spi2_MOSI_i,
	output 	logic						spi2_MISO_o,
	output	logic						m_drdy_o,	//fpga_DRDY_o, 	//	готовность
	output	logic						m_busy_o,	//	pin_32
	output	logic						hall_mayak_o,
	output	logic						fpga_E00_o,
	
//	TS4231	
	input		logic 					ts_ch_rs_i,
	input		logic 					ts_ch_next_i,
	input		logic 					ts_D_en_i,
	input		logic 					ts_E_en_i,
	
	inout 	logic [(INPUTS-1):0] D_io,				//	24
	inout 	logic [(INPUTS-1):0] E_io,				//	24
	inout		logic 					ts_E_io,
	inout		logic 					ts_D_io,

//	номер маяка для опроса и приема пакета
	input		logic [2:0] 			m_cod_i,			//	pin_113,pin_110,pin_111
	input		logic 					m_set_i,			//	pin_114
	input		logic 					m_reset_i,		//	pin_118
	input		logic 					m_get_i,			//	pin_119

//	светодиоды	
	output	logic						led_21,
	output	logic						led_22,
	output	logic						led_25,
	output	logic						led_97,

	output  	bit 						dip_6				//	test
);
	
//	test signal
assign hall_mayak_o = hall_mayak_i;	

assign fpga_E00_o   = E_io[0];	//	вывод E00 для 0-го канала

// =================  межблочные сигналы  =================
// ====  для симуляции должны быть в самом начале  ========
	logic [7:0]		net_m_set;
	logic [7:0]		net_m_reset;
	logic [7:0]		net_m_get;	

	logic [7:0]		net_drdy;
	logic [7:0]		net_busy;
	//logic 			net_spi_miso;
	logic				net_nRF_rs;		//	короткий импульс по срезу nRF
	
	logic [15:0] 	net_main_clk_0;//	для датчиков нулевого канала
	logic [15:0] 	net_main_clk_1;//	для датчиков первого канала
	logic				net_4mhz_0;
	logic				net_4mhz_1;
	
//=================	cfg ts4231 ===========================
	logic [7:0] chan_ts;
//	разложим 24 D_io сигнала на in и out	
	logic [(INPUTS-1):0] D_i;//	=	'z;	
	logic	[(INPUTS-1):0] D_o;//	=	'z;
	logic [(INPUTS-1):0] E_i;//	=	'z;	
	logic [(INPUTS-1):0] E_o;//	=	'z;
	
//=========================================================
	
sync_mod sync_module
(	
	.nRF_irq_in		(nRF_IRQ_i),	//	interrupts from nRF_2401P
	.clk_48m_in		(clk_48m_i),	//	48 mHz
	.mcod_in			(m_cod_i),		//	[2:0] код канала маяка
	.mset_in			(m_set_i),  	//	строб для записи кода маяка
	.mreset_in		(m_reset_i),	//	строб для сброса маяка
	
	.nRF_rs_out		(net_nRF_rs),	//	запуск temp_clk по срезу nRF
	.m_set_out		(net_m_set),	//	запись от STM выбранного маяка
	.m_rs_out		(net_m_reset),	// сброс от STM выбранного маяка
	.m_get_out		(net_m_get),	//	прием информации от выбранного маяка
	
//	информация от всех маяков		
	.ch_busy_in		(net_busy),
	.ch_drdy_in		(net_drdy),
	
// связь с STM	
	.mget_in			(m_get_i),		//	строб для чтения кодов от выбранного маяка
	.ch_drdy_out	(m_drdy_o),
	.ch_busy_out	(m_busy_o)
   
);

//==============================================================
logic [15:0]	time_stamp_0[(INPUTS-1):0];
logic [15:0]	time_stamp_1[(INPUTS-1):0];

logic [15:0]	time_stamp_out_0[(INPUTS-1):0];
logic [15:0]	time_stamp_out_1[(INPUTS-1):0];

/* перепишем все таймстемпы по сигналу готовности */
   always_ff@( posedge net_drdy[0] )
	  time_stamp_out_0 = time_stamp_0;
	  
	always_ff@( posedge net_drdy[1] )
	  time_stamp_out_1 = time_stamp_1;  

mayak_mod mayak_module_0
(
   .clk_48m_in		(clk_48m_i),		//	48 mHz
	.nRF_rs_in		(net_nRF_rs),   	// старт temp_clk по срезу nRF_irq
	.chset_in		(net_m_set[0]),	//	старт в нашем канале
	.chrs_in			(net_m_reset[0]),	//	сброс нашего канала

	.chdrdy_out		(net_drdy[0]),		//	data_ready
	.chbusy_out		(net_busy[0]),		//	mayak is busy
	.main_clk_out	(net_main_clk_0)	//	основной счетчик для всех датчиков 
	
);
//---------------------------------------------------------
mayak_mod mayak_module_1
(
   .clk_48m_in		(clk_48m_i),		//	48 mHz
	.nRF_rs_in		(net_nRF_rs),   	// старт temp_clk по срезу nRF_irq
	.chset_in		(net_m_set[1]),	//	старт в нашем канале
	.chrs_in			(net_m_reset[1]),	//	сброс нашего канала

	.chdrdy_out		(net_drdy[1]),		//	data_ready
	.chbusy_out		(net_busy[1]),		//	mayak is busy
	.main_clk_out	(net_main_clk_1)	//	основной счетчик для всех датчиков 
	
);
//===============================================================	
/* канал 0 для маяка с нечетным кодом 21 */	
ts4231_module #(21) ts4231_mod_0[(INPUTS-1):0]
(
	.rs_in			(net_m_set[0]),	//	"взводим" датчики канала
	.clk_48m_in		(clk_48m_i),		//	48 mHz
	.main_clk_in	(net_main_clk_0),			
	.D_in				(D_o),	
	.E_in				(E_o),	
	.times_out		(time_stamp_0)		// сюда пишем выходной код
);
/* канал 1 для маяка с четным кодом 12 */
ts4231_module #(12) ts4231_mod_1[(INPUTS-1):0]
(
	.rs_in			(net_m_set[1]),	//	"взводим" датчики канала
	.clk_48m_in		(clk_48m_i),		//	48 mHz
	.main_clk_in	(net_main_clk_1),			
	.D_in				(D_o),	
	.E_in				(E_o),	
	.times_out		(time_stamp_1)		// сюда пишем выходной код
);
//===============================================================	
//	Организуем SPI вывод
spi_module spi_mod
(
   .clk_48m_in		(clk_48m_i),	//	48MHz
	.spi_cs_in		(spi2_CS_i),
	.spi_clk_in		(spi2_SCK_i),  
	.spi_mosi_in	(spi2_MOSI_i),
	.mcod_in       (m_cod_i),	   //	код канала маяка
	.mget_in       (m_get_i),	   //	строб для чтения 
	
	.times0_in		(time_stamp_out_0),	//	маяк 0
	.times1_in		(time_stamp_out_1),	// маяк 1	
	.spi_miso_out	(spi2_MISO_o)

);
	
//=========== индикаторы ==================================
	int 	led_clk;
/*	мигаем светодиодoм постоянно */
	always_ff @ (posedge clk_48m_i)
		led_clk <= led_clk + 1;
	assign led_97 = led_clk[23];
//---------------------------------------------------------	
	int	nRF_led_cnt;
/*	подсветим наличие импульсов от nRF */
	always_ff @ (negedge led_clk[20], negedge nRF_IRQ_i)	
	if (nRF_IRQ_i == 0)
		begin nRF_led_cnt = 20; led_21 = 1; end	
	else
		if (nRF_led_cnt > 0)
			nRF_led_cnt = nRF_led_cnt-1;			
		else
			led_21 = 0;
//----------------------------------------------------------
/* подсветим наличие маяка_#0 */
	int	mayak_0_led_cnt;
	always_ff @ (posedge led_clk[20], posedge net_drdy[0])	
	if ( net_drdy[0] )
		begin	mayak_0_led_cnt = 20; led_22 = 1; end	
	else
		if (mayak_0_led_cnt > 0)
			mayak_0_led_cnt = mayak_0_led_cnt-1;			
		else
			led_22 = 0;
//----------------------------------------------------------
/* подсветим наличие маяка_#1 */
	int	mayak_1_led_cnt;
	always_ff @ (posedge led_clk[20], posedge net_drdy[1])	
	if ( net_drdy[1] )
		begin	mayak_1_led_cnt = 20; led_25 = 1;	end	
	else
		if (mayak_1_led_cnt > 0)
			mayak_1_led_cnt = mayak_1_led_cnt-1;			
		else
			led_25 = 0;
//==========================================================
//------------- Конфигурация ts4231 ------------------------
	///logic [7:0] chan_ts;
//	разложим 24 D_io сигнала на in и out	
	///logic [(INPUTS-1):0] D_i;//	=	'z;	
	///logic	[(INPUTS-1):0] D_o;//	=	'z;
	assign D_io = ((ts_D_en_i == 0) ? 'z : D_i);
	assign D_o	= D_io;
	assign D_i 	= ((ts_D_en_i) ? {INPUTS{ts_D_io}} : 'bz);
	
//	разложим 24 E_io сигнала на in и out
	//logic [(INPUTS-1):0] E_i;//	=	'z;	
	//logic [(INPUTS-1):0] E_o;//	=	'z;
/*	СИгнал был на выходе D прямой полярности, а после конфигурации
	огибающая на выходе "Е" в инверсной полярности, на выходе D
	модулированный сигнал лазера для декодирования */	
	
	assign E_io = ((ts_E_en_i == 0) ? 'z : E_i);
	assign E_o	= E_io;

	assign E_i 	= ((ts_E_en_i) ? {INPUTS{ts_E_io}} : 'bz);	
	assign ts_D_io = ((ts_D_en_i == 0) ? D_o[chan_ts] : 1'bz);
	assign ts_E_io = ((ts_E_en_i == 0) ? E_o[chan_ts] : 1'bz);
	
//	Установим следующий канал ts4231	
	
	always_ff@(posedge ts_ch_rs_i, posedge ts_ch_next_i)
	if (ts_ch_rs_i)
		chan_ts = 0;
	else
		chan_ts = chan_ts + 1;
//==========================================================
//----------------------------------------------------------
endmodule

