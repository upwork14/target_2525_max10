`include "defines.sv"

module ts4231_module 
#(parameter par_cod = 12)	//	код модуляции лазера
(
	input  logic  			rs_in,			//	чистим только наш канал
	input  logic         clk_48m_in,		//	48 mHz
	input	 logic [15:0] 	main_clk_in,	//	
	input  logic  			D_in,	
	input  logic  			E_in,
	output logic [15:0] 	times_out		//	выходной код
);
//=============================================================
//===============  локальные переменные  ======================
	logic	 			net_rdy;					//	готовность таймстемпа
	logic [15:0] 	cnt_front_E;			//	таймер до фронта Е
	logic [2:0]		E_shift;
	logic 			D_shift;
	logic [7:0] 	freq;
	logic [11:0]   timer_D;		//	частота модуляции НТС 1.9 МГц, у нас - 4 МГц
	logic [11:0]   timer_3M;
	bit  				start_E;
//------------------------------------------------		
	always_ff@( posedge clk_48m_in )
	begin
	  E_shift[2] <= E_shift[1]; 
	  E_shift[1] <= E_shift[0]; 
	  E_shift[0] <= E_in; 
	  D_shift    <= D_in;
	end
	
	assign start_E = E_shift[2] & (~E_shift[0]);
//------------------------------------------------	
/*	"взводим" датчик или сформируем сигнал готовности датчика */
	always_ff@( posedge rs_in, posedge E_shift[2] )
	if (rs_in)
	  net_rdy = 0;
	else
	  if (( freq == par_cod ) && ( timer_3M < timer_D ))
	    net_rdy = 1;

//------------------------------------------------	
//	запишем время до начала сигнала Е
	always_ff@( negedge E_shift[0] )
	  cnt_front_E <= main_clk_in;
	  
//------------------------------------------------	
/* частота модуляции лазера НТС_old */
	always_ff@( posedge start_E, posedge main_clk_in[0] )
	  if ( start_E )
	    timer_3M = 0;
	  else	 
	    ++timer_3M;
//------------------------------------------------	
/* частота модуляции нашего лазера 4 МГц */
	always_ff@( posedge start_E, posedge D_shift )
	  if ( start_E )
	    timer_D = 0;
	  else	 
	    ++timer_D;

//===============================================================	
/*	переписываем на выход по SPI по фронту общей готовности
   только при совпадении кода модуляции */	
	always_ff@( posedge rs_in, posedge net_rdy )
	if ( rs_in )
     times_out = 0;
	else if (( freq == par_cod ) && ( timer_3M < timer_D ))
	  times_out[15:0] = (cnt_front_E[15:1]) + (main_clk_in[15:1]);
	 
	else
	  times_out = 0;
	  
//===============================================================	 
	bit [11:0] cnt_D_one, cnt_D_zero;//, delta;
	always_ff@( posedge clk_48m_in )
	begin
	  if ( E_shift[2] ) 
	    begin
	      cnt_D_one  <= 0;
	      cnt_D_zero <= 0;
	    end
	  else if ( D_shift ) 
	    ++cnt_D_one; // = cnt_D_one  + 1;
	  else  
	    ++cnt_D_zero;// = cnt_D_zero + 1;
   end
//------------------------------------------	
	always_ff@ ( posedge E_shift[0] )	
	if ( cnt_D_one > cnt_D_zero )
	  freq = 21;
	else
     freq = 12;
//------------------------------------------	
/* найдем разницу длительности "1" и "0". А так как у нас в пакете
длительность "1" и "0" отличается в два раза, то порог ставим в 4 раза */
/*
	always_ff@ ( posedge E_shift[1] )
	  if ( cnt_D_one > cnt_D_zero )
	    delta = ( cnt_D_one - cnt_D_zero ) >> 1;
	  else
	    delta = ( cnt_D_zero - cnt_D_one ) >> 1;
*/
//===============================================================	

endmodule : ts4231_module

