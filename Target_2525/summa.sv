
module summa 
(
	input  logic a,
	input  logic b,
	input	 logic en,
	output logic [7:0] c
);

initial begin
	c = 32;
end
	
always_comb begin				  
	$display("\n Hello, world");
	if (en)
		c = a | b;
	else
		c = 8'bz;
end
				  
endmodule
