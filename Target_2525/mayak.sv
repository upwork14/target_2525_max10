`include "defines.sv"

module mayak_mod
(
	input  logic         clk_48m_in,		//	48 mHz
	input	 logic 			nRF_rs_in, 		// старт temp_clk по срезу nRF_irq
	input	 logic 			chset_in,		//	старт в нашем канале
	input	 logic 			chrs_in,			//	сброс нашего канала

	output logic 			chdrdy_out,  	//	data_ready
	output logic 			chbusy_out,  	//	mayak is busy
	output logic [15:0]	main_clk_out	//	основной счетчик для всех датчиков 
);
//=========================================================	
  logic [20:0] temp_clk = 0;
/*	временный счетчик, работает только при nRF_IR == 0 */
  always_ff@(posedge nRF_rs_in, posedge clk_48m_in)
  if (nRF_rs_in)
	 temp_clk = 0;
  else
    temp_clk = temp_clk+1;

//=========================================================	
/*	основной счетчик канала (Маяка) */
  int main_clk;  
    
	always_ff@( posedge chset_in, posedge clk_48m_in )	 
	begin
	  if (chset_in)
	    main_clk = temp_clk;
	  else 
	    if ( main_clk < CNT_READY ) //	8.08 ms
         main_clk = main_clk + 1;
	end
//	main_clk_out формируется только если был сигнал "СТАРТ"
	assign main_clk_out = main_clk[18:3];//main_clk[17:2];	//	6 MHz
//=========================================================	
/*  формируем готовность канала */	
	bit stop_main_cnt;	
/*	 имульс для включения готовности канала */
	assign stop_main_cnt = (main_clk == (CNT_READY-2));
/*  сброс готовности только от STM */	
	always_ff@( posedge chrs_in, posedge stop_main_cnt )
	if (chrs_in)
	  chdrdy_out = 0;
	else
	  chdrdy_out = 1;
	  
//=========================================================	
/*  формируем занятость канала */	
/*  начинается от chset_in (канал от nRF может не совпадать и
    стoИт до фронта готовности */	
	always_ff@( posedge chset_in, posedge chdrdy_out )
	if (chset_in)
	  chbusy_out = 1;
	else
	  chbusy_out = 0;
	  
//=========================================================		
/* example assign y = {c[2:1], {3{d[0]}}, c[0], 3’b101}; */
//------------------------------------------------------------
endmodule : mayak_mod
