
`include "defines.sv"

`timescale 1ps / 1ps

	module tb_ts4231 #(parameter par_cod = 21);	//	код модуляции лазера

	logic  			rs_in;			//	start_nRF
	logic          clk_48m_in;		//	48 mHz
	logic [15:0] 	main_clk_in;	//	
	logic  			D_in;
	logic  			E_in;
	logic [15:0] 	times_out;		//	выходной код

	ts4231_module #(21) DUT( .*);

  always begin
    #(1000000/96);		//	48 MHz
	 clk_48m_in = ~clk_48m_in;
  end
  
  always begin
	 D_in = '1; //~D_in;
	 #160ns;
	 D_in = '0;
	 #80ns;
	 
  end
  
  int clk;
  always_ff@( posedge clk_48m_in )
	 clk <= clk+1;
  
  assign main_clk_in = clk[18:3];
  
  initial begin
	 
	 clk_48m_in = 1;
	 rs_in    	= 0;
	 D_in			= 0;
	 E_in			= 1;
	 #1500ns;
	 rs_in		= 1; #40ns;	rs_in = 0;
    #500ns;
	 E_in			= 0;
	 #2000ns;
	 E_in			= 1;	 
	 #400ns; 
	 
	 #200ns;
	 
  end

  initial begin
    //#10ms;
	 #10000ns;
    $stop;
  end
/*
 for (int i=0; i<32; i++)
	 begin
	 
		FPGA_CS = 0;
		#100ns;
		for (int k=0; k<16; k++)
		begin
			#50ns;
			FPGA_SCK = 0;
			#50ns;
			FPGA_SCK = 1;
		end
		#100ns;
		FPGA_CS = 1;
		#100ns;
	 end

*/ 
endmodule

