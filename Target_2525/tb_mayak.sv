
`include "defines.sv"

`timescale 1ps / 1ps

	module tb_mayak;	//	код модуляции лазера

   logic          clk_48m_in;		//	48 mHz
	logic 			nRF_rs_in; 		// старт temp_clk по срезу nRF_irq
	logic 			chset_in;		//	старт в нашем канале
	logic 			chrs_in;			//	сброс нашего канала

	logic 			chdrdy_out;  	//	data_ready
	logic 			chbusy_out;  	//	mayak is busy
	logic [15:0]	main_clk_out;	//	основной счетчик для всех датчиков 	
   
	mayak_mod DUT( .*);

  always begin
    #(1000000/96);		//	48 MHz
	 clk_48m_in = ~clk_48m_in;
  end
  
  initial begin
	 nRF_rs_in    = 0;
	 chset_in     = 0;
	 chrs_in      = 0;
	 clk_48m_in   = 0;
	 #200ns;
	 chrs_in = 1; #100ns; chrs_in = 0;
	 #200ns;
	 nRF_rs_in = 1; #100ns; nRF_rs_in = 0;
	 #1000ns;
	 chset_in = 1; #100ns; chset_in = 0;
	 #500ns;
	 #9ms;
	 chrs_in = 1; #100ns; chrs_in = 0;
  end

  initial begin
    #10ms;
	 //#1ms;
	 //#100000ns;
    $stop;
  end

endmodule

