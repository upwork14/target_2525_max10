`ifndef DEFINES_SV
  `define DEFINES_SV
  
  package defines;
	 
	 parameter N_MAYAK	= 2;	//	количество маяков
	 parameter N_CHANS	= 1;	// 4	количество сигналов для FPGA_DRDY
	 parameter INPUTS  	= 24;	//	32;
    parameter WIDTH   	= 16;
	 //parameter CNT_READY	= 42000;	// 6.999ms
	 //parameter CNT_READY	= 47000;	// 7.82ms
	 //parameter CNT_READY	= 48000;	// 7.97ms
	 parameter CNT_READY	= (48500 * 8);	// 8.08ms
	 
    typedef enum logic[2:0] 	 
	 {
      IDLE			= 3'd1,
		WORK			= 3'd2,
		CNT_FRONT	= 3'd3, 
		CNT_SREZ		= 3'd4,	
		READY			= 3'd5
    } channel_state;
	 
	 typedef enum logic [2:0] 
	 {
      WAIT      = 3'd1,
		READ		 = 3'd2,
      FULL      = 3'd3      
    } programm_state;
//------------------------------------------------------------
/*
//	структура для приема информации от одного Маяка
	 typedef struct
	 {
		logic		 	 clk_en;				// разрешение счета
		logic [15:0] main_clk;			//	основной счетчик канала
		logic	[2:0]  num;					//	номер маяка 
		logic			 xy;					//	ось Х или У
		logic [15:0] stamp[INPUTS];	//	таймштампы датчиков
		logic			 drdy;				//	готовность данных
	 
	 } mayak_str;
*/	 
//------------------------------------------------------------	 	 
//	определим структуры для каждого Маяка 4шт.	 
	//mayak_str MAYAK[7:0];	
//------------------------------------------------------------	 
  endpackage
  
  import defines::*;
  
`endif // DEFINES_SV
