
`include "defines.sv"

`timescale 1ps / 1ps

	module tb_sync;	//	код модуляции лазера

   logic 		nRF_irq_in;	//	interrupts from nRF_2401P
	logic       clk_48m_in;	//	48 mHz
	logic [2:0]	mcod_in;		//	код канала маяка
	logic       mset_in;		//	строб для перезаписи таймера
	logic       mreset_in;	//	строб для сброса маяка
	logic       mget_in;		//	строб для чтения busy, drdy
	
	logic 		nRF_rs_out;	//	запуск temp_clk по срезу nRF
	logic [7:0]	m_set_out;	//	запись от STM выбранного маяка
	logic [7:0]	m_rs_out;	//	сброс от STM выбранного маяка
	logic [7:0]	m_get_out;	//	прием кода от выбранного маяка
	
//	информация от всех маяков	
	logic [7:0]	ch_busy_in;	//	занятость каналов при опросе
	logic [7:0]	ch_drdy_in; //	готовность всех каналов при опросе
		
	logic 		ch_drdy_out;// готовность выбранного маяка
	logic 		ch_busy_out;// маяк занят 
   
	sync_mod DUT( .*);

  always begin
    #(1000000/96);		//	48 MHz
	 clk_48m_in = ~clk_48m_in;
  end
  
  initial begin
    nRF_irq_in   = 1;
	 ch_busy_in   = '0;
	 ch_drdy_in   = '0;
	 mget_in      = 0; mset_in = 0; mreset_in = 0; mcod_in = 7;
	 clk_48m_in   = 0;
	 #200ns;
	 mset_in		  = 1; #80ns;	mset_in = 0;
	 #200ns;
	 mget_in		  = 1; #80ns;	mget_in = 0;
	 #200ns;
	 mreset_in	  = 1; #80ns;	mreset_in = 0;
	 #200ns;
	 nRF_irq_in   = 0;
	 #1000ns;
	 nRF_irq_in   = 1;
	 #200ns;
	 ch_busy_in   = '0;
	 ch_drdy_in   = '0;
	 
	 for (int i=0; i<8; i++)
	 begin
		mcod_in	 = i;
		#200ns;
		mset_in   = 1; #80ns;	mset_in = 0;
		#200ns;
		mget_in	 = 1; #80ns;	mget_in = 0;
	   #200ns;
		mreset_in = 1; #80ns;	mreset_in = 0;
	   #200ns;
	 end

	 #200ns;
  end

  initial begin
    //#10ms;
	 #1ms;
	 //#100000ns;
    $stop;
  end

endmodule

