
`include "defines.sv"

`timescale 1ps / 1ps

	module tb_top;

	logic						clk_48m_i; 
	logic 					nRF_IRQ_i;		//	--|______|-- from nRF
//	SPI2_interface
	logic						spi2_CS_i;
	logic						spi2_SCK_i;
	logic						spi2_MOSI_i;
	logic						spi2_MISO_o;
	logic						m_drdy_o;//fpga_DRDY_o; 	//	готовность
	logic						m_busy_o;
//	TS4231	
	logic 					ts_ch_rs_i;
	logic 					ts_ch_next_i;
	logic 					ts_D_en_i;
	logic 					ts_E_en_i;
	
	wire [(INPUTS-1):0]  D_io;				//	24
	wire [(INPUTS-1):0]  E_io;				//	24
	wire	 					ts_E_io;
	wire						ts_D_io;

//	номер маяка для опроса и приема пакета
	logic [2:0] 			m_cod_i;			//	pin_113,pin_110,pin_111
	logic 					m_set_i;			//	pin_114
	logic 					m_reset_i;		//	pin_118
   logic 					m_get_i;			//	pin_119
//	светодиоды	
	logic						led_21;
	logic						led_22;
	logic						led_25;
	logic						led_97;
	bit 						dip_6;				

//----------- inout ports ----------------
	logic [(INPUTS-1):0] d_input;
	logic 					d_upr;
	logic [(INPUTS-1):0] e_input;
	logic 					e_upr;
	
	assign D_io = (d_upr==1) ? d_input : 'z;
	assign E_io = (e_upr==1) ? e_input : 'z;
//----------------------------------------	

	top DUT( .*);

  always begin
    #(1000000/96);		//	48 MHz
	 clk_48m_i = ~clk_48m_i;
  end
  
  initial begin
	 m_cod_i = 0; m_set_i = 0; m_reset_i = 0; m_get_i = 0;
	 d_upr 		= 1;
	 e_upr 		= 1;
	 e_input 	= '1; 
	 d_input 	= '0;
	 
	 clk_48m_i  = 1;
	 nRF_IRQ_i  = 1;
	 m_set_i    = 0;
    #100ns;
	 m_cod_i    = 0;
	 #100ns;
	 nRF_IRQ_i  = 0;
	 #400ns;
	 m_set_i = 1; #60ns; m_set_i 	= 0;
	 #300ns;
	 
	 nRF_IRQ_i  = 1;
	 #100ns;
	 m_cod_i    = 1;
	 #100ns;
	 m_set_i = 1; #60ns; m_set_i 	= 0;
	 #100ns;
	 nRF_IRQ_i  = 0;
	 #800ns;
	 m_set_i = 1; #100ns; m_set_i = 0;
	 #200ns;
	 nRF_IRQ_i  = 1;
	 #100ns;
	 e_input = '0;
	 #100ns;
	 e_input = '1;
	 #400ns; 
	 //#9ms;
	 m_cod_i    = 0;
	 #200ns;
	 m_reset_i = 1; #100ns; m_reset_i = 0;
	 
	
  end

  initial begin
    //#10ms;
	 #5000ns;
    $stop;
  end
  
endmodule

