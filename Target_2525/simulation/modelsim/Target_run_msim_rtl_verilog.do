transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -sv -work work +incdir+F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525 {F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525/defines.sv}
vlog -sv -work work +incdir+F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525 {F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525/spi.sv}
vlog -sv -work work +incdir+F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525 {F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525/sync.sv}
vlog -sv -work work +incdir+F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525 {F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525/ts4231.sv}
vlog -sv -work work +incdir+F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525 {F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525/mayak.sv}
vlog -sv -work work +incdir+F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525 {F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525/top.sv}

vlog -sv -work work +incdir+F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525 {F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525/tb_ts4231.sv}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L fiftyfivenm_ver -L rtl_work -L work -voptargs="+acc"  tb_ts4231

do F:/_Deus_Moscow/_Horus/_Target/_Quartus_Target/_Project_target_2525/Target_2525/simulation/modelsim/wave.do
