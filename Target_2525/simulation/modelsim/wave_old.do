onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_top/FPGA_CE
add wave -noupdate /tb_top/MULT_CLK
add wave -noupdate /tb_top/MULT_DAT
add wave -noupdate /tb_top/FPGA_REZ_03
add wave -noupdate /tb_top/FPGA_REZ_04
add wave -noupdate /tb_top/FPGA_REZ_05
add wave -noupdate /tb_top/FPGA_REZ_06
add wave -noupdate /tb_top/TS4231_CLK
add wave -noupdate /tb_top/TS4231_DAT
add wave -noupdate -radix decimal -childformat {{{/tb_top/signal_i[31]} -radix decimal} {{/tb_top/signal_i[30]} -radix decimal} {{/tb_top/signal_i[29]} -radix decimal} {{/tb_top/signal_i[28]} -radix decimal} {{/tb_top/signal_i[27]} -radix decimal} {{/tb_top/signal_i[26]} -radix decimal} {{/tb_top/signal_i[25]} -radix decimal} {{/tb_top/signal_i[24]} -radix decimal} {{/tb_top/signal_i[23]} -radix decimal} {{/tb_top/signal_i[22]} -radix decimal} {{/tb_top/signal_i[21]} -radix decimal} {{/tb_top/signal_i[20]} -radix decimal} {{/tb_top/signal_i[19]} -radix decimal} {{/tb_top/signal_i[18]} -radix decimal} {{/tb_top/signal_i[17]} -radix decimal} {{/tb_top/signal_i[16]} -radix decimal} {{/tb_top/signal_i[15]} -radix decimal} {{/tb_top/signal_i[14]} -radix decimal} {{/tb_top/signal_i[13]} -radix decimal} {{/tb_top/signal_i[12]} -radix decimal} {{/tb_top/signal_i[11]} -radix decimal} {{/tb_top/signal_i[10]} -radix decimal} {{/tb_top/signal_i[9]} -radix decimal} {{/tb_top/signal_i[8]} -radix decimal} {{/tb_top/signal_i[7]} -radix decimal} {{/tb_top/signal_i[6]} -radix decimal} {{/tb_top/signal_i[5]} -radix decimal} {{/tb_top/signal_i[4]} -radix decimal} {{/tb_top/signal_i[3]} -radix decimal} {{/tb_top/signal_i[2]} -radix decimal} {{/tb_top/signal_i[1]} -radix decimal} {{/tb_top/signal_i[0]} -radix decimal}} -subitemconfig {{/tb_top/signal_i[31]} {-height 15 -radix decimal} {/tb_top/signal_i[30]} {-height 15 -radix decimal} {/tb_top/signal_i[29]} {-height 15 -radix decimal} {/tb_top/signal_i[28]} {-height 15 -radix decimal} {/tb_top/signal_i[27]} {-height 15 -radix decimal} {/tb_top/signal_i[26]} {-height 15 -radix decimal} {/tb_top/signal_i[25]} {-height 15 -radix decimal} {/tb_top/signal_i[24]} {-height 15 -radix decimal} {/tb_top/signal_i[23]} {-height 15 -radix decimal} {/tb_top/signal_i[22]} {-height 15 -radix decimal} {/tb_top/signal_i[21]} {-height 15 -radix decimal} {/tb_top/signal_i[20]} {-height 15 -radix decimal} {/tb_top/signal_i[19]} {-height 15 -radix decimal} {/tb_top/signal_i[18]} {-height 15 -radix decimal} {/tb_top/signal_i[17]} {-height 15 -radix decimal} {/tb_top/signal_i[16]} {-height 15 -radix decimal} {/tb_top/signal_i[15]} {-height 15 -radix decimal} {/tb_top/signal_i[14]} {-height 15 -radix decimal} {/tb_top/signal_i[13]} {-height 15 -radix decimal} {/tb_top/signal_i[12]} {-height 15 -radix decimal} {/tb_top/signal_i[11]} {-height 15 -radix decimal} {/tb_top/signal_i[10]} {-height 15 -radix decimal} {/tb_top/signal_i[9]} {-height 15 -radix decimal} {/tb_top/signal_i[8]} {-height 15 -radix decimal} {/tb_top/signal_i[7]} {-height 15 -radix decimal} {/tb_top/signal_i[6]} {-height 15 -radix decimal} {/tb_top/signal_i[5]} {-height 15 -radix decimal} {/tb_top/signal_i[4]} {-height 15 -radix decimal} {/tb_top/signal_i[3]} {-height 15 -radix decimal} {/tb_top/signal_i[2]} {-height 15 -radix decimal} {/tb_top/signal_i[1]} {-height 15 -radix decimal} {/tb_top/signal_i[0]} {-height 15 -radix decimal}} /tb_top/signal_i
add wave -noupdate /tb_top/meandr_i
add wave -noupdate /tb_top/led_o
add wave -noupdate -radix decimal /tb_top/DUT/sync_module/inputs_o
add wave -noupdate /tb_top/start_i
add wave -noupdate {/tb_top/DUT/sync_module/inputs_o[31]}
add wave -noupdate -radix unsigned {/tb_top/DUT/ts4231[0]/clk_front}
add wave -noupdate {/tb_top/DUT/sync_module/inputs_o[1]}
add wave -noupdate -radix unsigned {/tb_top/DUT/ts4231[1]/cod_out}
add wave -noupdate -radix unsigned {/tb_top/DUT/ts4231[1]/clk_front}
add wave -noupdate {/tb_top/DUT/sync_module/inputs_o[2]}
add wave -noupdate -radix unsigned {/tb_top/DUT/ts4231[2]/cod_out}
add wave -noupdate -radix unsigned {/tb_top/DUT/ts4231[2]/clk_front}
add wave -noupdate /tb_top/FPGA_DRDY
add wave -noupdate /tb_top/FPGA_MISO
add wave -noupdate /tb_top/FPGA_SCK
add wave -noupdate /tb_top/DUT/spi_mod/cnt_02
add wave -noupdate /tb_top/DUT/spi_mod/cnt_01
add wave -noupdate /tb_top/DUT/spi_mod/cs_front
add wave -noupdate /tb_top/FPGA_CS
add wave -noupdate {/tb_top/DUT/sync_module/inputs_o[0]}
add wave -noupdate -radix decimal -childformat {{{/tb_top/DUT/sync_module/cnt[0][31]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][30]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][29]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][28]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][27]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][26]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][25]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][24]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][23]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][22]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][21]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][20]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][19]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][18]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][17]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][16]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][15]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][14]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][13]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][12]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][11]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][10]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][9]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][8]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][7]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][6]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][5]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][4]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][3]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][2]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][1]} -radix hexadecimal} {{/tb_top/DUT/sync_module/cnt[0][0]} -radix hexadecimal}} -subitemconfig {{/tb_top/DUT/sync_module/cnt[0][31]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][30]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][29]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][28]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][27]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][26]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][25]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][24]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][23]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][22]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][21]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][20]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][19]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][18]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][17]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][16]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][15]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][14]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][13]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][12]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][11]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][10]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][9]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][8]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][7]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][6]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][5]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][4]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][3]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][2]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][1]} {-height 15 -radix hexadecimal} {/tb_top/DUT/sync_module/cnt[0][0]} {-height 15 -radix hexadecimal}} {/tb_top/DUT/sync_module/cnt[0]}
add wave -noupdate -radix unsigned /tb_top/DUT/main_clk
add wave -noupdate /tb_top/clk_48m_i
add wave -noupdate -radix unsigned {/tb_top/DUT/ts4231[0]/cod_out}
add wave -noupdate /tb_top/DUT/test
add wave -noupdate -radix decimal {/tb_top/signal_i[0]}
add wave -noupdate {/tb_top/signal_i[1]}
add wave -noupdate {/tb_top/signal_i[2]}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6703476170 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 249
configure wave -valuecolwidth 166
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {10500 us}
