onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix unsigned /tb_ts4231/DUT/clk_48m_in
add wave -noupdate -radix unsigned /tb_ts4231/DUT/D_in
add wave -noupdate -radix unsigned /tb_ts4231/DUT/D_shift
add wave -noupdate -radix unsigned /tb_ts4231/DUT/E_shift
add wave -noupdate -radix unsigned /tb_ts4231/DUT/freq
add wave -noupdate -radix unsigned /tb_ts4231/DUT/net_rdy
add wave -noupdate -radix unsigned /tb_ts4231/DUT/par_cod
add wave -noupdate -radix unsigned /tb_ts4231/DUT/E_in
add wave -noupdate -radix unsigned /tb_ts4231/DUT/rs_in
add wave -noupdate -radix unsigned /tb_ts4231/DUT/main_clk_in
add wave -noupdate -radix unsigned /tb_ts4231/DUT/cnt_front_E
add wave -noupdate -radix unsigned /tb_ts4231/DUT/times_out
add wave -noupdate -radix unsigned /tb_ts4231/DUT/cnt_D_one
add wave -noupdate -radix unsigned /tb_ts4231/DUT/cnt_D_zero
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2894522 ps} 0} {{Cursor 2} {802981 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 198
configure wave -valuecolwidth 56
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1272418 ps} {4907912 ps}
