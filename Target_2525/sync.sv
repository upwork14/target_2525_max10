
`include "defines.sv"

module sync_mod
(	
	input	 logic 			nRF_irq_in,	//	interrupts from nRF_2401P
	input  logic         clk_48m_in,	//	48 mHz
	input  logic [2:0]	mcod_in,		//	код канала маяка
	input  logic         mset_in,		//	строб для перезаписи таймера
	input  logic         mreset_in,	//	строб для сброса маяка
	input  logic         mget_in,		//	строб для чтения busy, drdy
	
	output logic 			nRF_rs_out,	//	запуск temp_clk по срезу nRF
	output logic [7:0]	m_set_out,	//	запись от STM выбранного маяка
	output logic [7:0]	m_rs_out,	//	сброс от STM выбранного маяка
	output logic [7:0]	m_get_out,	//	прием кода от выбранного маяка
	
//	информация от всех маяков	
	input  logic [7:0]	ch_busy_in,	//	занятость каналов при опросе
	input  logic [7:0]	ch_drdy_in,	//	готовность всех каналов при опросе
		
	output logic 			ch_drdy_out,// готовность выбранного маяка
	output logic 			ch_busy_out	// маяк занят 
);

//===============================================================
	logic [2:0] start_sync;
	logic 		shift[2:0];
	bit 		   net_mset_in[1:0];
	bit 		   net_mreset_in[1:0];
	bit 		   net_mget_in[1:0];
	
//	инвертируем синхроимпульс от nRF, делаем его коротким
// и используем в качестве стартового сигнала для всех маяков
	
	always_ff @ ( posedge clk_48m_in )
	begin
		shift[2]         <= shift[1]; 
		shift[1]         <= shift[0];     		shift[0]         <= nRF_irq_in;
		net_mset_in[1]   <= net_mset_in[0];    net_mset_in[0]   <= mset_in;
		net_mreset_in[1] <= net_mreset_in[0];  net_mreset_in[0] <= mreset_in;
		net_mget_in[1]   <= net_mget_in[0] ;   net_mget_in[0]   <= mget_in;
	end
	
//	старт импульс от nRF длительностью 40nc	
	assign nRF_rs_out = (~shift[0]) &  shift[2];	//	srez of nRF
	
//---------------------------------------------------------
	bit [2:0] m_num_set;
	bit [2:0] m_num_reset;
	bit [2:0] m_num_get;
	
/* запишем номер канала (маяка), с которым работаем */	
	always_ff@( posedge net_mset_in[0] )
		m_num_set = mcod_in;
		
	always_ff@( posedge net_mreset_in[0] )
		m_num_reset = mcod_in;
		
	always_ff@( posedge net_mget_in[0] )
		m_num_get = mcod_in;	
		
//	старт выбранного маяка (по номеру mcod_in, полученному от STM)
	assign m_set_out  = (net_mset_in[1]) ? ( 1<<m_num_set ) : 0;

//	сброс выбранного маяка (по номеру mcod_in, полученному от STM)
	assign m_rs_out   = (net_mreset_in[1]) ? ( 1<<m_num_reset ) : 0;
	
//	выбор маяка для опроса кода (по номеру mcod_in, полученному от STM)
	assign m_get_out  = (net_mget_in[1]) ? ( 1<<m_num_get ) : 0;
//---------------------------------------------------------	
//	формируем коды drdy и busy
	assign ch_drdy_out = ( ch_drdy_in[m_num_get] ) ? 1 : 0;
	assign ch_busy_out = ( ch_busy_in[m_num_get] ) ? 1 : 0;
//===============================================================	
endmodule
