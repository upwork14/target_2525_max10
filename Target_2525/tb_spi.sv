
`include "defines.sv"

`timescale 1ps / 1ps

	module tb_spi;	//	код модуляции лазера

   logic        clk_48m_in;		//	48MHz
	logic        spi_cs_in;
	logic        spi_clk_in;  
	logic        spi_mosi_in;
	logic [2:0]	 mcod_in;		   //	код канала маяка
	logic        mget_in;	      //	строб для чтения 
	
	logic [15:0] times0_in [(INPUTS-1):0];		//	маяк 4МГц
	logic [15:0] times1_in [(INPUTS-1):0];		// маяк 8МГц	
	logic			 spi_miso_out;

	spi_module DUT( .*);

  always begin
    #(1000000/96);		//	48 MHz
	 clk_48m_in = ~clk_48m_in;
  end
  
  initial begin
  
	 for (int i=0; i<24; i++)
	 begin
		times0_in[i] = 16'hAA00 + i;
		times1_in[i] = 16'h5500 + i;
	 end	
	 mget_in      = 0;
	 clk_48m_in   = 0;
	 spi_clk_in   = 0;
	 spi_cs_in    = 1;
	 spi_mosi_in  = 0;
	 mcod_in		  = 1;
	 #100ns;
	 mget_in		  = 1; #80ns;	mget_in = 0;
	 #200ns;
	
    for (int i=0; i<24; i++)
	 begin
	 
		spi_cs_in = 0;
		#100ns;
		for (int k=0; k<16; k++)
		begin
			#100ns;
			spi_clk_in = 1;
			#100ns;
			spi_clk_in = 0;
		end
		#100ns;
		spi_cs_in = 1;
		#100ns;
	 end
	 
  end

  initial begin
    //#10ms;
	 #1ms;
	 //#100000ns;
    $stop;
  end

endmodule

